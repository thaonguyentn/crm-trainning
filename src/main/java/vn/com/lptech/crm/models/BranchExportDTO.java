package vn.com.lptech.crm.models;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;
import vn.com.lptech.crm.configurations.annotation.ExportExcel;
import vn.com.lptech.crm.domains.Branch;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class BranchExportDTO extends BaseAuditableDTO implements Serializable {

  private String id;

  @ExportExcel(title = "Tên chi nhánh")
  private String name;

  @ExportExcel(title = "Mã chi nhánh")
  private String code;

  @ExportExcel(title = "Địa chỉ chi nhánh")
  private String address;

  @ExportExcel(title = "Thời gian mở cửa")
  private Date openTime;

  @ExportExcel(title = "Số lượng nhân viên")
  private Integer employees;

  public BranchExportDTO(Branch branch) {
    if (!ObjectUtils.isEmpty(branch)) {
      this.setId(String.valueOf(branch.getId()));
      this.setCode(branch.getCode());
      this.setName(branch.getName());
      this.setCreatedBy(branch.getCreatedBy());
      this.setCreatedDate(branch.getCreatedDate());
      this.setUpdatedBy(branch.getUpdatedBy());
      this.setUpdatedDate(branch.getUpdatedDate());
    }
  }
}
