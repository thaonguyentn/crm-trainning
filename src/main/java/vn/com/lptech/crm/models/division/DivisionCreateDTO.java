package vn.com.lptech.crm.models.division;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
/**
 * @author: hai
 * @date: 15/09/2021
 * @Note: mục đích: map dữ liệu khi tạo mới hoặc sửa khối.
 */
public class DivisionCreateDTO {
    @NonNull private String code;
    @NonNull private String name;
    @NonNull private String customer_type;
}
