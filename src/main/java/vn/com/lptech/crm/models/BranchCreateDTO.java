package vn.com.lptech.crm.models;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class BranchCreateDTO {
  @NonNull private String code;
  @NonNull private String name;




}
