package vn.com.lptech.crm.models.division;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;
import vn.com.lptech.crm.configurations.annotation.ExportExcel;
import vn.com.lptech.crm.domains.Division;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
/**
 * @author: hai
 * @date: 15/09/2021
 * @Note: mục đích: map dữ liệu đầu ra mong muốn.
 */
public class DivisionDTO  implements Serializable {

  @ExportExcel(title = "Tên khối")
  private String code;
  @ExportExcel(title = "Mã khối")
  private String name;
  @ExportExcel(title = "Kiểu khách hàng")
  private String customer_type;

  public DivisionDTO(Division division) {
    if (!ObjectUtils.isEmpty(division)) {
      this.setCode(division.getCode());
      this.setName(division.getName());
      this.setCustomer_type(division.getCustomerType());
    }
  }

}
