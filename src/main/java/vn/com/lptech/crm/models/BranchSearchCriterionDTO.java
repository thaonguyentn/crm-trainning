package vn.com.lptech.crm.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
@Builder
public class BranchSearchCriterionDTO {

  private String code;
  private String name;
 
  public BranchSearchCriterionDTO(String code, String name) {
    this.setCode(code);
    this.setName(name);
  }
}
