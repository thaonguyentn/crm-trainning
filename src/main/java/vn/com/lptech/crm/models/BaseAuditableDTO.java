package vn.com.lptech.crm.models;

import java.time.LocalDate;
import java.util.Date;
import lombok.Data;

@Data
public class BaseAuditableDTO {

  private String createdBy;

  private LocalDate createdDate;

  private String updatedBy;

  private LocalDate updatedDate;
}
