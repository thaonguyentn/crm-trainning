package vn.com.lptech.crm.models;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;
import vn.com.lptech.crm.domains.Branch;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class BranchDTO extends BaseAuditableDTO implements Serializable {

  private String id;
  private String code;
  private String name;

  public BranchDTO(Branch branch) {
    if (!ObjectUtils.isEmpty(branch)) {
      this.setId(String.valueOf(branch.getId()));
      this.setCode(branch.getCode());
      this.setName(branch.getName());
      this.setCreatedBy(branch.getCreatedBy());
      this.setCreatedDate(branch.getCreatedDate());
      this.setUpdatedBy(branch.getUpdatedBy());
      this.setUpdatedDate(branch.getUpdatedDate());

    }
  }
}
