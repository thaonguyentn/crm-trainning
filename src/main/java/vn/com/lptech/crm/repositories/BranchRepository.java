package vn.com.lptech.crm.repositories;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import vn.com.lptech.crm.domains.Branch;

public interface BranchRepository extends PagingAndSortingRepository<Branch, Long>,
    JpaSpecificationExecutor<Branch> {

  Optional<Branch> findTopByCodeOrderByCode(String code);

  List<Branch> findAllByOrderByCode();

  boolean existsBranchByCode(String code);
}
