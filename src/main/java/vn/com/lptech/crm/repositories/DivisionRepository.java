package vn.com.lptech.crm.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import vn.com.lptech.crm.domains.Division;

import java.util.List;
import java.util.Optional;

/**
 * @author: hai
 * @date: 15/09/2021
 */
public interface DivisionRepository extends PagingAndSortingRepository<Division, Long> {
    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: tìm kiếm khối theo mã khối
     */
    Optional<Division> findByCode(String code);

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: tìm kiếm khối theo tên khối
     */
    List<Division> findByName(String name);

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: kiểm tra tồn tại của khối theo mã khối.
     */
    boolean existsDivisionByCode(String code);
}
