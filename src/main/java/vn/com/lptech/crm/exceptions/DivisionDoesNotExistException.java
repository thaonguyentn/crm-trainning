package vn.com.lptech.crm.exceptions;

/**
 * @author: hai
 * @date: 15/09/2021
 */
public class DivisionDoesNotExistException extends ApplicationException{
    public DivisionDoesNotExistException() {
        super("mã khối không tồn tại");
    }
}
