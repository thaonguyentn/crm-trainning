package vn.com.lptech.crm.exceptions;

public class BranchAlreadyExistException extends ApplicationException {
  public BranchAlreadyExistException() {
    super("Chi nhánh đã tồn tại");
  }
}
