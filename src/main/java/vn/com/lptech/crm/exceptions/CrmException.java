package vn.com.lptech.crm.exceptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author duongnhathuy
 * @version 1.0
 * @since 15/09/2021 Class CrmException kế thừa Exception phục vụ việc xử lý và tổng hợp lỗi trong
 * sourceCode CRM.
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class CrmException extends Exception {

  private List<String> messages;
  private Throwable throwable;

  public CrmException(String msg) {
    this.messages = new ArrayList<String>(Arrays.asList(msg));
  }

  public CrmException(String msg, Throwable throwable) {
    this.messages = new ArrayList<String>(Arrays.asList(msg));
    this.throwable = throwable;
  }

  public CrmException(List msgs) {
    this.messages = msgs;
  }

}
