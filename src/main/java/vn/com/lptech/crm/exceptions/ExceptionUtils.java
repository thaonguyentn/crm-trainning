package vn.com.lptech.crm.exceptions;

import java.util.HashMap;
import java.util.Map;

public class ExceptionUtils {

  public static final String E_INTERNAL_SERVER = "E_INTERNAL_SERVER";
  public static final String E_COMMON_DUPLICATE_CODE = "E_COMMON_DUPLICATE_CODE";
  public static final String E_COMMON_NOT_EXISTS_CODE = "E_COMMON_NOT_EXISTS_CODE";
  public static final String E_EXPORT_001 = "E_EXPORT_001";

  public static Map<String, String> messages;

  static {
    messages = new HashMap<>();
    messages.put(ExceptionUtils.E_INTERNAL_SERVER, "Server không phản hồi");
    messages.put(ExceptionUtils.E_COMMON_DUPLICATE_CODE, "Code %s đã tồn tại");
    messages.put(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE, "Code %s không tồn tại");
    messages.put(ExceptionUtils.E_EXPORT_001, "Export excel rỗng");

  }

  public static String buildMessage(String messKey, Object... arg){
    return String.format(ExceptionUtils.messages.get(messKey), arg);
  }
}
