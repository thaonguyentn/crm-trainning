package vn.com.lptech.crm.exceptions;

/**
 * @author: hai
 * @date: 15/09/2021
 */
public class DivisionCodeAlreadyExistException  extends ApplicationException{
    public DivisionCodeAlreadyExistException() {
        super("mã khối đã tồn tại");
    }
}
