package vn.com.lptech.crm.exceptions;

public class BranchDoesNotExistException extends ApplicationException {
  public BranchDoesNotExistException() {
    super("Chi nhánh không tồn tại");
  }
}
