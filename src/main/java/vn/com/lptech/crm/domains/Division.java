package vn.com.lptech.crm.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;


/**
 * @author: hai
 * @date: 15/09/2021
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "division")
public class Division extends Auditable<String>{
    @Id
    @SequenceGenerator(
            name = "division_id_sequence",
            sequenceName = "division_id_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "division_id_sequence")
    @Column(name = "id")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "customer_type")
    private String customerType;

    @CreatedBy
    @Column(name = "created_by")
    private String createdBy;


    @CreatedDate
    @Column(name = "created_date", updatable = false)
    private LocalDate createdDate;

    @LastModifiedBy
    @Column(name = "updated_by")
    private String updatedBy;

    @LastModifiedDate
    @Column(name = "updated_date", updatable = false)
    private LocalDate updatedDate;
}
