package vn.com.lptech.crm.domains;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {

  @CreatedBy
  @Column(name = "created_by", nullable = false, updatable = false)
  private U createdBy;

  @CreatedDate
  @Column(name = "created_date", nullable = false, updatable = false)
  private LocalDate createdDate;

  @LastModifiedBy
  @Column(name = "updated_by")
  private U updatedBy;

  @LastModifiedDate
  @Column(name = "updated_date")
  private LocalDate updatedDate;
}
