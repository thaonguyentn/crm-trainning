package vn.com.lptech.crm.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "branch")
public class Branch extends Auditable<String> {

  @Id
  @SequenceGenerator(
      name = "branch_id_sequence",
      sequenceName = "branch_id_sequence",
      allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "branch_id_sequence")
  @Column(name = "id")
  private Long id;

  @Column(name = "code", nullable = false)
  private String code;

  @Column(name = "name")
  private String name;

  @CreatedBy
  @Column(name = "created_by")
  private String createdBy;

  @CreatedDate
  @Column(name = "created_date")
  private LocalDate createdDate;

  @LastModifiedBy
  @Column(name = "updated_by")
  private String updatedBy;

  @LastModifiedDate
  @Column(name = "updated_date")
  private LocalDate updatedDate;
}
