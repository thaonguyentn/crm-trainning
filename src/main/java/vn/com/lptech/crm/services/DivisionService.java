package vn.com.lptech.crm.services;
import vn.com.lptech.crm.models.division.DivisionCreateDTO;
import vn.com.lptech.crm.models.division.DivisionDTO;

import java.util.List;

/**
 * @author: hai
 * @date: 15/09/2021
 */
public interface DivisionService {
    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: xem tất cả các khối
     */
    List<DivisionDTO> findAll() throws Exception;

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: thêm mới 1 khối.
     */
    void save(DivisionCreateDTO divisionCreateDTO) throws Exception;

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: chỉnh sửa 1 khối
     */
    void update(DivisionCreateDTO dto, Long id) throws Exception;

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: tìm kiếm khối theo mã khối
     */
    DivisionDTO findByCode(String code) throws Exception;

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: tìm kiếm khối theo tên khối
     */
    List<DivisionDTO> findByName(String name) throws Exception;

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: xóa khối theo mã khối
     */
    void delete(String code) throws Exception;

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: xuất dữ liệu ra file excel
     */
    void exportExcel() throws Exception;
}