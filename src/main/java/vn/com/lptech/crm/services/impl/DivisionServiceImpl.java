package vn.com.lptech.crm.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.com.lptech.crm.domains.Division;
import vn.com.lptech.crm.exceptions.CrmException;
import vn.com.lptech.crm.exceptions.DivisionDoesNotExistException;
import vn.com.lptech.crm.exceptions.ExceptionUtils;
import vn.com.lptech.crm.models.division.DivisionCreateDTO;
import vn.com.lptech.crm.models.division.DivisionDTO;
import vn.com.lptech.crm.repositories.DivisionRepository;
import vn.com.lptech.crm.services.DivisionService;
import vn.com.lptech.crm.utils.ExcelUtils;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author: hai
 * @date: 15/09/2021
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class DivisionServiceImpl implements DivisionService {
    private final ModelMapper modelMapper;
    private final DivisionRepository repository;


    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: xem tất cả các khối
     */
    @Override
    public List<DivisionDTO> findAll() {
        /** tạo 1 list DTO để lưu dữ liệu trả về   */
        List<DivisionDTO> dtoList = new ArrayList<>();

        /** tạo 1 list Khối để lưu dữ liệu lấy đc từ database   */
        List<Division> divisionList = (List<Division>) repository.findAll();

        /** nếu dữ liệu lấy được mà null thì return hàm là 1 list DTO trống  */
        if (CollectionUtils.isEmpty(divisionList)) return dtoList;

        /** add tất cả các dữ liệu lấy đc từ database vào list dữ liệu trả về.   */
        for (Division division : divisionList) {
            dtoList.add(new DivisionDTO(division));
        }
        return dtoList;
    }

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: thêm mới 1 khối.
     */
    @Override
    public void save(DivisionCreateDTO divisionCreateDTO) throws CrmException {

        /** kiểm tra mã code của khối cần tạo mới đã tồn tại trên database chưa .
         * nếu tồn tại rồi thì ném a ngoại lệ, còn không thì tiếp tục,. */
        if (repository.existsDivisionByCode(divisionCreateDTO.getCode())){
                throw new CrmException(String.format(ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_DUPLICATE_CODE),
                        divisionCreateDTO.getCode()));

        }
        /** chuyển đổi từ divisionCreateDTO sang Division để map với dữ liệu bảng trên DB để insert bản ghi mới.*/
        Division division = new Division();
        division.setCode(divisionCreateDTO.getCode());
        division.setName(divisionCreateDTO.getName());
        division.setCustomerType(divisionCreateDTO.getCustomer_type());
        repository.save(division);
        }


    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: chỉnh sửa 1 khối.
     */
    @Override
    public void update(DivisionCreateDTO dto, Long id) {
        /** kiểm tra tồn tại của khối cần chỉnh sửa, nếu tồn tại thì tiếp tục, còn không thì ném ra ngoại lệ*/
        Optional<Division> division = repository.findById(id);
        if (division.isPresent()){
      division.get().setCode(dto.getCode());
      division.get().setName(dto.getName());
      division.get().setCustomerType(dto.getCustomer_type());
      repository.save(division.get());

        }
        else throw new DivisionDoesNotExistException();
    }


    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: tìm kiếm theo mã khối.
     */
    @Override
    public DivisionDTO findByCode(String code) {
        /** kiểm tra tồn tại của khối, nếu không tồn tại thì ném ra ngoại lệ.*/
        Optional<Division> division = repository.findByCode(code);
        if (division.isEmpty()){
            throw new DivisionDoesNotExistException();
        }
        /** nếu tồn tại thì lấy ra khối đó và map với DTO để tạo dữ liệu đầu ra*/
        Division division1 = division.get();
        return new DivisionDTO(division1);
    }

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: tìm kiếm theo tên khối.
     */
    @Override
    public List<DivisionDTO> findByName(String name) {
        /** Tạo 1 list DTO để lưu dư liệu đầu ra*/
        List<DivisionDTO> divisionDTOList = new ArrayList<>();

        /** Tạo 1 list  để lưu dư liệu lấy được trên database*/
        List<Division> divisionList = repository.findByName(name);

        /**  nếu dữ liệu trên DB trống thì return list rỗng.,*/
        if (CollectionUtils.isEmpty(divisionList)) return divisionDTOList;

        /** còn không thì chuyển kiểu dữ liệu lấy đc trên DB sang kiểu DTO*/
        for (Division division : divisionList) {
            divisionDTOList.add(new DivisionDTO(division));
        }

        return divisionDTOList;
    }

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: Xóa khối theo mã khối. */
    @Override
    public void delete(String code) {
        /**  tìm kiếm khối theo mã khối*/
        Optional<Division> division = repository.findByCode(code);

        /** nếu tồn tại thì xóa. */
     if (division.isPresent()){
        repository.delete(division.get());
     }
         /** không thì ném ra ngoại lệ */
    else {
         throw new DivisionDoesNotExistException();
     }

    }

    /**
     * @author: hai
     * @date: 16/09/2021
     * @note: xuất ra file excel. */
    @Override
    public void exportExcel() throws Exception {
        /** tạo list lưu dữ liệu cần xuất ra excel. */
        List<Division> divisionList = (List<Division>) repository.findAll();

        /** nếu list dữ liệu rỗng thì ném ra ngoại lệ. */
        if (CollectionUtils.isEmpty(divisionList)) {
            throw new CrmException(ExceptionUtils.E_EXPORT_001);
        }

        /** chuyển đổi list dữ liệu sang kiểu DTO cho dữ liệu đầu ra mong muốn. */
        List<DivisionDTO> dtoList = new ArrayList<>();
        for (Division division : divisionList) {
            dtoList.add(new DivisionDTO(division));
        }

        /** chuyển đổi list kia sang kiểu object để xuất đucợ file*/
        List<Object> exports = new ArrayList<>(dtoList);

        /** Tạo 1 workbook*/
        Workbook workbook = ExcelUtils.executeExport((exports));

        /** tạo đường dẫn để lưu dữ liệu. */
        FileOutputStream outputStream = new FileOutputStream(
                "D:/writeExcel/test.xlsx");
        workbook.write(outputStream);
    }
}
