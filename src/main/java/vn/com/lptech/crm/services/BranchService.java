package vn.com.lptech.crm.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.lptech.crm.models.BranchCreateDTO;
import vn.com.lptech.crm.models.BranchDTO;
import vn.com.lptech.crm.models.BranchSearchCriterionDTO;

import java.util.List;

/**
 * @author: vanna
 * @date: 27/08/2021
 */
public interface BranchService {

  BranchDTO findByCode(String code) throws Exception;

  void create(BranchCreateDTO dto) throws Exception;

  void delete(String code) throws Exception;

  List<BranchDTO> findAll() throws Exception;

  void exportExcel() throws Exception;

  Page<BranchDTO> findByCriteria(BranchSearchCriterionDTO criterionDTO, Pageable pageable)
      throws Exception;
}
