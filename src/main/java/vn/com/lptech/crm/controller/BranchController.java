package vn.com.lptech.crm.controller;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.com.lptech.crm.exceptions.CrmException;
import vn.com.lptech.crm.exceptions.ExceptionUtils;
import vn.com.lptech.crm.models.BranchCreateDTO;
import vn.com.lptech.crm.models.BranchDTO;
import vn.com.lptech.crm.models.BranchSearchCriterionDTO;
import vn.com.lptech.crm.services.BranchService;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RequestMapping("/branches")
@RestController
public class BranchController {

  private final BranchService service;

  /**
   * @author: vanna
   * @date: 27/08/2021
   * @note: Tìm kiếm thông tin chi nhánh theo mã chi nhánh
   */
  @GetMapping(value = "/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> findByCode(@PathVariable String code) {
    BranchDTO branchDTO = null;
    try {
      branchDTO = service.findByCode(code);
    } catch (CrmException ex) {
      return new ResponseEntity<>(
          ex.getMessages(), HttpStatus.OK); // Lỗi nghiệp vụ nên trả về HTTPStatus = 200 (OK)
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus
              .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
    }
    return new ResponseEntity<>(branchDTO, HttpStatus.OK);
  }

  /**
   * @author: vanna
   * @date: 27/08/2021
   * @note: Liệt kê toàn bộ danh sách thông tin chi nhánh GET /crm01-trainning/branchs
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> findAll() {
    List<BranchDTO> branchs = null;
    try {
      branchs = service.findAll();
    } catch (CrmException ex) {
      return new ResponseEntity<>(
          ex.getMessages(), HttpStatus.OK); // Lỗi nghiệp vụ nên trả về HTTPStatus = 200 (OK)
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus
              .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
    }
    return new ResponseEntity<>(branchs, HttpStatus.OK);
  }

  /**
   * @author: vanna
   * @date: 27/08/2021
   * @note: Tạo mới chi nhánh.
   */
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> create(@RequestBody BranchCreateDTO dto) {
    try {
      service.create(dto);
    } catch (CrmException ex) {
      return new ResponseEntity<>(ex.getMessages(), HttpStatus.OK);
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.CREATED);
  }

  /**
   * @author: vanna
   * @date: 27/08/2021
   * @note: Xoá chi nhánh.
   */
  @DeleteMapping(value = "/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> delete(@PathVariable String code) {
    try {
      service.delete(code);
    } catch (CrmException ex) {
      return new ResponseEntity<>(
          ex.getMessages(), HttpStatus.NO_CONTENT); // HTTPStatus của Delete là NO_CONTENT
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }

  /**
   * @author: vanna
   * @date: 27/08/2021
   * @note: Export toàn bộ danh sách thông tin chi nhánh
   */
  @GetMapping(value = "/export", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> export() {
    try {
      service.exportExcel();
    } catch (CrmException ex) {
      return new ResponseEntity<>(
          ex.getMessages(), HttpStatus.OK); // Lỗi nghiệp vụ nên trả về HTTPStatus = 200 (OK)
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus
              .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }


  /**
   * @author: vanna
   * @date: 27/08/2021
   * @note: Liệt kê toàn bộ danh sách thông tin chi nhánh theo tiêu chí
   */
  @GetMapping(value = "/criteria", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> findByCriteria(
      @RequestParam(value = "code", required = false) String code,
      @RequestParam(value = "name", required = false) String name,
      @Parameter(hidden = true) Pageable pageable) {

    BranchSearchCriterionDTO searchCriterionDTO = null;
    if (code != null || name != null) {
      searchCriterionDTO = new BranchSearchCriterionDTO(code, name);
    }
    try {
      Page<BranchDTO> response = service.findByCriteria(searchCriterionDTO, pageable);
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (CrmException ex) {
      return new ResponseEntity<>(
          ex.getMessages(), HttpStatus.OK); // Lỗi nghiệp vụ nên trả về HTTPStatus = 200 (OK)
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus
              .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
    }
  }

}
