package vn.com.lptech.crm.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.com.lptech.crm.exceptions.CrmException;
import vn.com.lptech.crm.exceptions.ExceptionUtils;
import vn.com.lptech.crm.models.division.DivisionCreateDTO;
import vn.com.lptech.crm.models.division.DivisionDTO;
import vn.com.lptech.crm.services.DivisionService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


/**
 * @author: hai
 * @date: 15/09/2021
 */
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/divisions")
@RestController
public class DivisionController {
  private final DivisionService service;


  /**
   * @author: hai
   * @date: 16/09/2021
   * @note: xem tất cả các khối
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> listDivisions() throws IOException {
    List<DivisionDTO> divisionDTOList = null;
    try {
      divisionDTOList = service.findAll();
    } catch (CrmException ex) {
      return new ResponseEntity<>(ex.getMessages(), HttpStatus.OK);
    } catch (Exception e) {
      e.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>(divisionDTOList, HttpStatus.OK);
  }

  /**
   * @author: hai
   * @date: 16/09/2021
   * @note: tìm kiếm khối theo mã khối
   */
  @GetMapping( value = "/byCode/{code}" ,produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findByCode(@PathVariable String code) {
    Optional<DivisionDTO> divisionDTO = null;
    try {
      divisionDTO = Optional.ofNullable(service.findByCode(code));
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus
              .INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
    }
    return new ResponseEntity<>(divisionDTO, HttpStatus.OK);
  }

  /**
   * @author: hai
   * @date: 16/09/2021
   * @note: tìm kiếm khối theo tên khối
   */
  @GetMapping(value = "/byName/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> findByName(@PathVariable String name){
    List<DivisionDTO> divisionDTOList;
    try{
      divisionDTOList = service.findByName(name);
    }catch (Exception e){
      e.printStackTrace();
      return new ResponseEntity<>(
              ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE),
              HttpStatus.INTERNAL_SERVER_ERROR
      );
    }
    return new ResponseEntity<>(divisionDTOList, HttpStatus.OK);
  }


  /**
   * @author: hai
   * @date: 16/09/2021
   * @note: tạo mới 1 khối
   */
  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createDivision(@RequestBody DivisionCreateDTO dto) {
    try {
      service.save(dto);
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
    log.info("thanh công");
    return new ResponseEntity<>(HttpStatus.CREATED);
  }

  /**
   * @author: hai
   * @date: 16/09/2021
   * @note: chỉnh sửa khối
   */
  @PutMapping("/{id}")
  public ResponseEntity<?> updateDivision(@PathVariable Long id, @RequestBody DivisionCreateDTO dto) {
    try{
      service.update(dto, id);
    }catch (Exception e){
      e.printStackTrace();
      return new ResponseEntity<>(ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
              HttpStatus.INTERNAL_SERVER_ERROR);
    }
    log.info("thanh công");
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * @author: hai
   * @date: 16/09/2021
   * @note: Xóa khối
   */
  @DeleteMapping(value = "/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> deleteDivision(@PathVariable String code) {
    try {
      service.delete(code);
    } catch (CrmException ex) {
      return new ResponseEntity<>(
          ex.getMessages(), HttpStatus.NO_CONTENT); // HTTPStatus của Delete là NO_CONTENT
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(
          ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }


  /**
   * @author: hai
   * @date: 16/09/2021
   * @note: xuất tất cả khối ra file excel
   */
  @GetMapping(value = "/export", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> export() {
    try {
      service.exportExcel();
    } catch (CrmException ex) {
      return new ResponseEntity<>(ex.getMessages(),
              HttpStatus.OK); // Lỗi nghiệp vụ nên trả về HTTPStatus = 200 (OK)
    } catch (Exception ex) {
      ex.printStackTrace();
      return new ResponseEntity<>(ExceptionUtils.messages.get(ExceptionUtils.E_INTERNAL_SERVER),
              HttpStatus.INTERNAL_SERVER_ERROR); // Lỗi hệ thống nên trả về HTTPStatus = 500 (INTERNAL_SERVER)
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }


}

